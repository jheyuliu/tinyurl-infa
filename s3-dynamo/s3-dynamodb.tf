# S3 Buckets
resource "aws_s3_bucket" "dev-bucket" {
  bucket = "terraform-aws-eks-for-jheyu"

  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket_object" "dev-bucket-eks" {
  bucket  = aws_s3_bucket.dev-bucket.id
  key     = "dev/eks-cluster/"
  content = ""
}

resource "aws_s3_bucket_object" "dev-bucket-lb" {
  bucket  = aws_s3_bucket.dev-bucket.id
  key     = "dev/aws-lb/"
  content = ""
}

# DynamoDB
resource "aws_dynamodb_table" "dev-eks-cluster" {
  name           = "dev-eks"
  read_capacity  = 5
  write_capacity = 5

  hash_key = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}

resource "aws_dynamodb_table" "dev-lb-controller" {
  name           = "dev-lb-controller"
  read_capacity  = 5
  write_capacity = 5

  hash_key = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}